
const express = require("express");

// app - server
const app = express();

const port = 3000;

// Middlewares - softwares that provide common services and capabilities to applications outside of what's offered by the operating system
app.use(express.json()); // Allows your app to read JSON data
app.use(express.urlencoded({extended: true})); // Allows your app to read data from any other forms

app.listen(port, () => console.log(`Server running at ${port}`));


//  [SECTION] ROUTES
// GET Method
app.get("/greet", (request, response) => {
	response.send("Hello from the /greet endpoint!");
});

// POST Method
app.post("/hello", (request, response) => {
	response.send(`Hello there, ${request.body.firstName} ${request.body.lastName}!`);
})


// Simple Registration Form

let users = [];


app.post("/signup", (request,response) => {

	if (request.body.username !== "" && request.body.password !==""){
		users.push(request.body);
		response.send(`User "${request.body.username}" successfully registered with password: ${request.body.password}!`)
	} else {
		response.send(`Please input a username and password!`);
	}

})

// Simple Change Password

app.patch("/change-password", (request,response) => {

	let message;

	if (users.length === 0) {
		message = "No users found.";
	}

	for(let i = 0; i < users.length; i++){
		if(request.body.username == users[i].username){
			users[i].password = request.body.password;
			message = `User ${request.body.username}'s password has been updated`;

			break; //do this since it's a loop - best practice
		} else {
			message = `User does not exist.`;
		}
	} 

	response.send(message);

})






/*===============================================================================================================================*/
/*=======================================================ACTIVITY================================================================*/
/*===============================================================================================================================*/

app.get("/home", (request, response) => {
	response.send("Welcome to the home page");
});



app.get("/users", (request, response) => {
	response.send(users);
});


app.delete("/delete-user", (request,response) => {

	let message;

	if (users.length === 0) {
		message = "No users found.";
	}

	for(let i = 0; i < users.length; i++){
		if(request.body.username == users[i].username){
			users.splice(i, 1);
			message = `User ${request.body.username} has been deleted`;

			break;
		} else {
			message = `User does not exist.`;
		}
	} 
	response.send(message);

});